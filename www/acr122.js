// Empty constructor
function ACR122() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
ACR122.prototype.listen = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(function(str) {
    var json = JSON.parse(str);
    successCallback(json);
  }, errorCallback, 'ACR122Plugin', 'listen', [options]);
}

ACR122.prototype.cancel = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'ACR122Plugin', 'cancel', [options]);
}

ACR122.prototype.start = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'ACR122Plugin', 'start', [options]);
}

ACR122.prototype.stop = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'ACR122Plugin', 'stop', [options]);
}

// Installation constructor that binds ACR122Plugin to window
ACR122.install = function() {
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.acr122 = new ACR122();
  return window.plugins.acr122;
};
cordova.addConstructor(ACR122.install);