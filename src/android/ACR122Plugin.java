package com.appivo.cordova.acr122;

import java.util.Map;

import org.apache.cordova.*;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.acs.smartcard.Reader;
import com.acs.smartcard.ReaderException;


public class ACR122Plugin extends CordovaPlugin {
    
    private final static String CMD_LISTEN = "listen";
    private final static String CMD_START  = "start";
    private final static String CMD_STOP   = "stop";
    private final static String CMD_CANCEL = "cancel";

    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private static final String[] stateStrings = {"Unknown", "Absent", "Present", "Swallowed", "Powered", "Negotiable", "Specific"};

    private Reader reader;
    private UsbDevice usbDevice;
    private UsbManager usbManager;
    private CallbackContext callback;
    private PendingIntent mPermissionIntent;
    private boolean requested;
    private BroadcastReceiver receiver;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

        // Get USB manager
        usbManager = (UsbManager) cordova.getActivity().getSystemService(Context.USB_SERVICE);

        // Register receiver for USB permission
        receiver = new BroadcastReceiver() {

            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                  synchronized (this) {
                        if (usbDevice == null) {
                            openUSBDevice();
                        }
                    }   
                } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                    synchronized (this) {
                        UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if (device != null && device.equals(reader.getDevice())) {
                            usbDevice = null;
                            if (reader != null) {
                                reader.close();
                            }
                        }
                    }
                } else if (ACTION_USB_PERMISSION.equals(action)) {
                    synchronized (this) {
                        requested = false;
                        UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            if (device != null) {
                                if (reader == null || !reader.isOpened()) {                                    
                                    createReader(device);
                                }
                            }
                        } 
                    }
                }
            }
        };

        mPermissionIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        getActivity().registerReceiver(receiver, filter);

        openUSBDevice();
     }

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {
        boolean ok = false;
        if (CMD_LISTEN.equals(action)) {
           listen(callbackContext);
            ok = true;
        } else if (CMD_CANCEL.equals(action)) {
            cancel(callbackContext);
            ok = true;
        }
        return ok;
    }

    @Override
    public void onPause(boolean multitasking) {
        if (!multitasking) {
            stopNfc();
        }
        super.onPause(multitasking);
    }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
        if (!multitasking) {
            startNfc();
        }
    }

    @Override
    public void onDestroy() {
        stopNfc();
        getActivity().unregisterReceiver(receiver);

        super.onDestroy();
    }

    private void listen(CallbackContext callbackContext) {
        PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);              
        callback = callbackContext;
    }

    private boolean openUSBDevice() {
        boolean ok = false;
        if (usbDevice != null) {
            ok = true;
            if (reader == null || !reader.isOpened()) {
                createReader(usbDevice);
            }
        } else {
            Map<String, UsbDevice> devices = usbManager.getDeviceList();
            if (!devices.isEmpty() && !requested) {
                UsbDevice device = devices.values().iterator().next();
                usbManager.requestPermission(device, mPermissionIntent);
                requested = true;
                ok = true;
            }   
        }     
        return ok;
    }

    private void createReader(UsbDevice device) {
        usbDevice = device;

        if (reader != null) {
            reader.close();
        }

        // Initialize reader
        reader = new Reader(usbManager);
        reader.setOnStateChangeListener(new Reader.OnStateChangeListener() {

            @Override
            public void onStateChange(int slotNumber, int previousState, int currentState) {
                if (previousState < Reader.CARD_UNKNOWN || previousState > Reader.CARD_SPECIFIC) {
                    previousState = Reader.CARD_UNKNOWN;
                }

                if (currentState < Reader.CARD_UNKNOWN || currentState > Reader.CARD_SPECIFIC) {
                    currentState = Reader.CARD_UNKNOWN;
                }

                String state = stateStrings[currentState];
                try {
                    JSONObject message = new JSONObject();
                    message.put("state", state);
                    if (currentState == Reader.CARD_PRESENT) {
                        byte[] sendBuffer = new byte[]{(byte) 0xFF, (byte) 0xCA, (byte) 0x0, (byte) 0x0, (byte) 0x0};
                        byte[] receiveBuffer = new byte[16];
                        int byteCount = reader.control(slotNumber, Reader.IOCTL_CCID_ESCAPE, sendBuffer, sendBuffer.length, receiveBuffer, receiveBuffer.length);

                        StringBuffer uid = new StringBuffer();
                        for (int i = 0; i < (byteCount - 2); i++) {
                            uid.append(String.format("%02X", receiveBuffer[i]));
                        }

                        JSONObject tag = new JSONObject();
                        tag.put("id", uid.toString());
                        message.put("tag", tag);
                     }

                    PluginResult result = new PluginResult(PluginResult.Status.OK, message.toString());
                    result.setKeepCallback(true);
                    if (callback != null) {
                        callback.sendPluginResult(result);                        
                    }
                } catch (Exception e) {
                    if (callback != null) {
                        PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                        result.setKeepCallback(true);
                        callback.sendPluginResult(result);
                    }
                    e.printStackTrace();
                }
            }
        });   

        reader.open(device);

        reset();
    }

    private void reset() {
        if (reader != null && reader.isOpened()) {
            int slots = reader.getNumSlots();
            if (slots > 0) {
               for (int i = 0; i < slots; i++) {
                    try {
                        reader.power(i, Reader.CARD_WARM_RESET);
                        reader.setProtocol(i, Reader.PROTOCOL_T0 | Reader.PROTOCOL_T1);

                        byte[] sendBuffer = {(byte)0xFF, (byte)0xCA, (byte)0x00, (byte)0x00, (byte)0x00};
                        byte[] recvBuffer = new byte[300];
                        reader.transmit(i, sendBuffer, sendBuffer.length, recvBuffer, recvBuffer.length);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void cancel(CallbackContext callbackContext) {
        callback = null;
    }

    private void startNfc() {
        openUSBDevice(); 
    }

    private void stopNfc() {        
        if (reader != null) {
            reader.close();
            reader = null;
        }
    }

    private Activity getActivity() {
        return cordova.getActivity();
    }

    private Intent getIntent() {
        return getActivity().getIntent();
    }
}