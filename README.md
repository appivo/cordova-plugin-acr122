# cordova-plugin-acr122

A Cordova plugin that enables integration with an external ACS ACR122U NFC-reader.

API:

listen(callback) - register a listener

cancel() - cancel the currently registered listener


Example:

```javascript
var nfc = window.plugins.acr122;

nfc.listen(function(event) {
    if (event.state == 'Present') {
        alert("Scanned tag with id " + event.tag.id);
    }
});
```